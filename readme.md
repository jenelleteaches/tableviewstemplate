Example of how to use a TableView. Shows:
* How to create a table view
* Mandatory tableview functions
* Clicking on a row
* Deleting on a row
